#Build and run
```bash
docker build -t skugrid --no-cache . && docker run -d --name skugrid_running -p 8080:8080 skugrid
```

# Handlers:

##/load_info 
Добавляет переданные url в очередь на загрузку. Возвращает request_id

* Method: *POST*
* Body example:
```json
[
	"https://www.amazon.co.uk/gp/product/1509836071",
	"https://www.amazon.co.uk/gp/product/B01GFPWTI4/",
	"https://www.amazon.co.uk/gp/product/B00UXG0H2M",
	"https://www.amazon.co.uk/gp/product/B00UXG0H2M/ref=s9u_ri_gw_i3?ie=UTF8&fpl=fresh&pd_rd_i=B005G39HUK&pd_rd_r=fffd596f-61f7-11e8-9a21-019e4b2648c4&pd_rd_w=HQZu6&pd_rd_wg=aBaXg&pf_rd_m=A3P5ROKL5A1OLE&pf_rd_s=&pf_rd_r=HFNNM4HCNGPB6NKC379Q&pf_rd_t=36701&pf_rd_p=11a081fb-f660-495d-a83d-2ce30891dbb6&pf_rd_i=desktop&th=1&psc=1"
]
```

##/get_info
Отдает загруженную информацию по request_id

* Method: *GET*
* Parameters:
    * request_id (int)
* Request example:
```curl
curl -X GET \
  'http://localhost:8080/get_info?request_id=1' \
  -H 'Cache-Control: no-cache' \
```