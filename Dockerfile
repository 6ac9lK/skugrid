FROM golang
WORKDIR /go/src/bitbucket.org/6ac9lK/skugrid/
COPY . .


RUN go install -v ./...
EXPOSE 8080
ENTRYPOINT ["skugrid"]