package handlers

import (
	"net/http"
	"strconv"

	"bitbucket.org/6ac9lK/skugrid/loader"
)

func GetGetInfoHandler() http.Handler {
	return &getInfoHandler{
		Loader: loader.GetLoader(),
	}
}

type getInfoHandler struct {
	Loader loader.ILoader
}

func (gih *getInfoHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		http.NotFound(w, req)
		return
	}

	idStr := req.URL.Query().Get("request_id")

	id, err := strconv.ParseUint(idStr, 10, 0)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	result, err := gih.Loader.GetResult(id)
	if err != nil {
		if _, ok := err.(loader.NotFoundError); ok {
			http.NotFound(w, req)
			return
		}
		http.Error(w, "Internal error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(result)
}
