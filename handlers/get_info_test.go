package handlers

import (
	"errors"
	"net/http"
	"net/http/httptest"

	"bitbucket.org/6ac9lK/skugrid/loader"
	"bitbucket.org/6ac9lK/skugrid/loader/mocks"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/ginkgo/extensions/table"
	. "github.com/onsi/gomega"
)

const (
	contentTypeJSON = "application/json"
	contentTypeText = "text/plain; charset=utf-8"
)

var _ = Describe("GetInfo", func() {
	var (
		handler    http.Handler
		mockCtrl   *gomock.Controller
		mockLoader *loader_mocks.MockILoader
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		mockLoader = loader_mocks.NewMockILoader(mockCtrl)
		handler = &getInfoHandler{Loader: mockLoader}

	})

	type mockParams struct {
		arg    uint64
		result []byte
		err    error
	}
	type expectedResult struct {
		status      int
		contentType string
	}

	DescribeTable("General requests", func(method string, url string, lr mockParams, er expectedResult) {
		mockLoader.EXPECT().GetResult(lr.arg).Return(lr.result, lr.err)

		req, err := http.NewRequest(method, url, nil)
		Expect(err).NotTo(HaveOccurred())
		Expect(req).NotTo(BeNil())

		w := httptest.NewRecorder()

		handler.ServeHTTP(w, req)

		Expect(w.Code).To(BeEquivalentTo(er.status))
		Expect(w.Body.String()).NotTo(BeEmpty())
		Expect(w.Header().Get("Content-Type")).To(BeEquivalentTo(er.contentType))
	},
		Entry(
			"Wrong HTTP method",
			"POST", "", mockParams{arg: 1, result: nil, err: nil}, expectedResult{status: http.StatusNotFound, contentType: contentTypeText},
		),
		Entry(
			"Wrong query",
			"GET", "?request_id=", mockParams{arg: 1, result: nil, err: nil}, expectedResult{status: http.StatusBadRequest, contentType: contentTypeText},
		),
		Entry(
			"Wrong query",
			"GET", "?request_id=string", mockParams{arg: 1, result: nil, err: nil}, expectedResult{status: http.StatusBadRequest, contentType: contentTypeText},
		),
		Entry(
			"Successful request",
			"GET", "?request_id=1", mockParams{arg: 1, result: []byte("test"), err: nil}, expectedResult{status: http.StatusOK, contentType: contentTypeJSON},
		),
		Entry(
			"Not existed request_id request",
			"GET", "?request_id=1", mockParams{arg: 1, result: nil, err: loader.NotFoundError("test")}, expectedResult{status: http.StatusNotFound, contentType: contentTypeText},
		),
		Entry(
			"Internal server error",
			"GET", "?request_id=1", mockParams{arg: 1, result: nil, err: errors.New("test")}, expectedResult{status: http.StatusInternalServerError, contentType: contentTypeText},
		),
	)
})
