package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/6ac9lK/skugrid/loader"
)

const urlsLimit = 100

func GetLoadInfoHandler() http.Handler {
	return &loadInfoHandler{
		Loader: loader.GetLoader(),
	}
}

type loadInfoHandler struct {
	Loader loader.ILoader
}

func (lih *loadInfoHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodPost {
		http.NotFound(w, req)
		return
	}

	if req.Body == nil {
		http.Error(w, "body is empty", http.StatusBadRequest)
		return
	}

	var urls []string
	if err := json.NewDecoder(req.Body).Decode(&urls); err != nil {
		http.Error(w, fmt.Sprintf("can't decode json: %v", err), http.StatusBadRequest)
		return
	}

	if len(urls) == 0 || len(urls) > urlsLimit {
		http.Error(
			w,
			fmt.Sprintf("Wrong array size. Minimum urls: %d; maximum: %d", 0, urlsLimit),
			http.StatusBadRequest,
		)
		return
	}

	reqId := lih.Loader.PushToQueue(urls)

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf("%d", reqId)))
}
