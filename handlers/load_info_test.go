package handlers

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"

	"bitbucket.org/6ac9lK/skugrid/loader/mocks"
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/ginkgo/extensions/table"
	. "github.com/onsi/gomega"
)

const loadInfoURL = "/load_info"

var _ = Describe("LoadInfo", func() {
	overSizedJSON, _ := json.Marshal(
		strings.Split(strings.Repeat("?", urlsLimit+1), ""),
	)
	var (
		handler    http.Handler
		mockCtrl   *gomock.Controller
		mockLoader *loader_mocks.MockILoader
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		mockLoader = loader_mocks.NewMockILoader(mockCtrl)
		handler = &loadInfoHandler{Loader: mockLoader}
	})
	DescribeTable("Wrong requests", func(method string, body io.Reader, code int) {
		req, err := http.NewRequest(method, loadInfoURL, body)
		Expect(err).NotTo(HaveOccurred())
		Expect(req).NotTo(BeNil())

		w := httptest.NewRecorder()

		handler.ServeHTTP(w, req)

		Expect(w.Code).To(BeEquivalentTo(code))
	},
		Entry("Wrong HTTP method", "GET", nil, http.StatusNotFound),
		Entry("Empty body", "POST", nil, http.StatusBadRequest),
		Entry("Invalid JSON", "POST", strings.NewReader(`[`), http.StatusBadRequest),
		Entry("Invalid JSON", "POST", strings.NewReader(`{}`), http.StatusBadRequest),
		Entry("Invalid JSON", "POST", strings.NewReader(`[]`), http.StatusBadRequest),
		Entry("Invalid JSON", "POST", bytes.NewReader(overSizedJSON), http.StatusBadRequest),
	)

	Context("Success context", func() {
		var body io.Reader
		BeforeEach(func() {
			body = strings.NewReader(`["test.url"]`)
		})
		DescribeTable("Success cases", func(expReqId uint64) {
			mockLoader.EXPECT().PushToQueue([]string{"test.url"}).Return(expReqId)
			req, err := http.NewRequest("POST", loadInfoURL, body)
			Expect(err).NotTo(HaveOccurred())
			Expect(req).NotTo(BeNil())

			w := httptest.NewRecorder()

			handler.ServeHTTP(w, req)

			Expect(w.Code).To(BeEquivalentTo(http.StatusOK))
			Expect(strconv.Atoi(w.Body.String())).To(BeEquivalentTo(expReqId))
		},
			Entry("First successful request", uint64(1)),
			Entry("Second successful request", uint64(2)),
			Entry("Third successful request", uint64(3)),
		)
	})
})
