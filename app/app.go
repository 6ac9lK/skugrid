package app

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/6ac9lK/skugrid/config"
	"bitbucket.org/6ac9lK/skugrid/handlers"
)

type App struct {
	Config *config.Config
}

func NewApp() (*App, error) {
	app := &App{}
	cfg, err := config.NewConfig()
	if err != nil {
		return nil, err
	}
	app.Config = cfg

	app.initHandlers()

	return app, nil
}

func (a *App) Run() {
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", a.Config.Port), nil))
}

func (a *App) initHandlers() {
	http.Handle("/load_info", handlers.GetLoadInfoHandler())
	http.Handle("/get_info", handlers.GetGetInfoHandler())
}
