package main

import (
	"bitbucket.org/6ac9lK/skugrid/app"
)

func main() {
	a, err := app.NewApp()
	if err != nil {
		panic(err)
	}

	a.Run()
}
