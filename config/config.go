package config

import (
	"errors"
	"flag"
	"fmt"

	"github.com/go-ini/ini"
)

const configPath = "etc/%s.ini"

type Config struct {
	Port uint
}

func NewConfig() (*Config, error) {
	var env string
	flag.StringVar(&env, "env", "dev", "Pass the env value(dev/prod)")
	flag.Parse()

	if env != "dev" && env != "prod" {
		return nil, errors.New("passed wrong env value")
	}

	cfgName := fmt.Sprintf(configPath, env)
	cfg, err := ini.Load(cfgName)
	if err != nil {
		return nil, err
	}

	c := &Config{
		Port: cfg.Section("server").Key("port").MustUint(8080),
	}

	return c, nil
}
