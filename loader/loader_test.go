package loader

import (
	"bytes"
	"encoding/json"
	"fmt"
	"sync"

	"bitbucket.org/6ac9lK/skugrid/loader/testdata"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/ginkgo/extensions/table"
	. "github.com/onsi/gomega"
	"gopkg.in/jarcoal/httpmock.v1"
)

var _ = Describe("Loader", func() {
	type asset struct {
		response []byte
		result   []byte
	}
	var (
		assets = map[string]*asset{"1": nil, "2": nil}
		l      loader
	)

	BeforeSuite(func() {
		httpmock.Activate()

		for url := range assets {
			resp, err := testdata.Asset(fmt.Sprintf("assets/%s-response.html", url))
			Expect(err).NotTo(HaveOccurred())
			Expect(resp).NotTo(BeEmpty())

			res, err := testdata.Asset(fmt.Sprintf("assets/%s-result.json", url))
			Expect(err).NotTo(HaveOccurred())
			Expect(res).NotTo(BeEmpty())

			assets[url] = &asset{response: resp, result: res}
		}

	})
	AfterSuite(func() {
		httpmock.DeactivateAndReset()
	})

	BeforeEach(func() {
		httpmock.Reset()
		for url, asset := range assets {
			httpmock.RegisterResponder("GET", url,
				httpmock.NewBytesResponder(200, asset.response))
		}

		l = loader{}
	})

	It("Test getNextId", func() {
		wg := sync.WaitGroup{}
		for i := 0; i < requestsLimit*10; i++ {
			wg.Add(1)
			go func() {
				l.getNextId()
				wg.Done()
			}()
		}
		wg.Wait()
		Expect(l.idCounter == requestsLimit).To(BeTrue())
	})

	Context("Load", func() {
		DescribeTable("Test load", func(url string) {
			var expResult Result
			Expect(json.Unmarshal(assets[url].result, &expResult)).To(Succeed())

			l.load(url)
			e, ok := l.cache.Load(url)
			Expect(ok).To(BeTrue())

			result, ok := e.(entry)
			Expect(ok).To(BeTrue())
			Expect(result.status).To(BeEquivalentTo(finished))
			Expect(result.Result).To(BeEquivalentTo(expResult))
		},
			Entry("Url '1'", "1"),
			Entry("Url '2'", "2"),
		)

		DescribeTable("Test parseResponse", func(url string) {
			var expResult Result
			Expect(json.Unmarshal(assets[url].result, &expResult)).To(Succeed())

			result, err := l.parseResponse(bytes.NewReader(assets[url].response), url)
			Expect(err).NotTo(HaveOccurred())
			Expect(result).To(BeEquivalentTo(expResult))
		},
			Entry("Url '1'", "1"),
			Entry("Url '2'", "2"),
		)
	})

	Context("Get context", func() {
		var (
			requests = map[uint64][]string{
				1:   {"1"},
				2:   {"2"},
				12:  {"1", "2"},
				123: {"1", "2", "3"},
			}
			results map[string]Result
		)
		BeforeEach(func() {
			for k, v := range requests {
				l.requests.Store(k, v)
			}
			results = map[string]Result{}
			for url, asset := range assets {
				var expResult Result
				Expect(json.Unmarshal(asset.result, &expResult)).To(Succeed())
				results[url] = expResult
				l.cache.Store(url, entry{Result: expResult})
			}
		})
		DescribeTable("Test GetResult", func(id uint64, urls []string, wantErr bool, expErr error) {
			var expResult []Result
			for _, url := range urls {
				res, ok := results[url]
				if !ok {
					res = Result{Err: NotFoundError("Not found")}
				}
				expResult = append(expResult, res)
			}

			json, err := json.Marshal(expResult)
			Expect(err).NotTo(HaveOccurred())
			Expect(json).NotTo(BeEmpty())

			res, err := l.GetResult(id)
			if wantErr {
				Expect(err).To(HaveOccurred())
			} else {
				Expect(err).NotTo(HaveOccurred())
				Expect(res).To(BeEquivalentTo(json))
			}
		},
			Entry("id=1", uint64(1), []string{"1"}, false, nil),
			Entry("id=2", uint64(2), []string{"2"}, false, nil),
			Entry("id=12", uint64(12), []string{"1", "2"}, false, nil),
			Entry("id=123", uint64(123), []string{"1", "2", "3"}, false, nil),
			Entry("id=0", uint64(0), nil, true, NotFoundError("Not found")),
		)
	})
})
