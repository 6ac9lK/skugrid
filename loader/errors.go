package loader

const (
	nfError  = "Not found"
	notReady = "Result not ready yet"
)

type NotFoundError string

func (nf NotFoundError) Error() string {
	return nfError
}

type NotReadyError string

func (nr NotReadyError) Error() string {
	return notReady
}

type BadResponseCode struct {
}

func (brc BadResponseCode) Error() string {
	return "Bad response code"
}

type InternalError struct {
}

func (ie InternalError) Error() string {
	return "Internal server error"
}

type ParseError struct {
	err string
}

func (pe ParseError) Error() string {
	return pe.err
}
