package loader

type Result struct {
	Err  error  `json:"error,omitempty"`
	Url  string `json:"url"`
	Meta struct {
		Title   string `json:"title"`
		Price   string `json:"price"`
		Image   string `json:"image"`
		InStock bool   `json:"in_stock"`
	} `json:"meta,omitempty"`
}

type entry struct {
	Result
	status int
}
