//go:generate mockgen -package loader_mocks -destination mocks/loader.go bitbucket.org/6ac9lK/skugrid/loader ILoader
package loader

import (
	"encoding/json"
	"io"
	"net/http"
	"strings"
	"sync"

	"github.com/PuerkitoBio/goquery"
)

const (
	requestsLimit = 1000

	inProgress = iota
	finished
)

var (
	loaderInstance ILoader
	once           sync.Once

	priceSelectors = []string{
		"#priceblock_ourprice",
		"#buyNewSection .offer-price",
	}
)

type ILoader interface {
	PushToQueue([]string) uint64
	GetResult(uint64) ([]byte, error)
}

func GetLoader() ILoader {
	once.Do(func() {
		loaderInstance = &loader{}
	})

	return loaderInstance
}

type loader struct {
	idCounter uint64
	idMu      sync.Mutex
	requests  sync.Map
	cache     sync.Map
}

func (l *loader) GetResult(id uint64) ([]byte, error) {
	e, ok := l.requests.Load(id)
	if !ok {
		return nil, NotFoundError(nfError)
	}

	urls, ok := e.([]string)
	if !ok {
		return nil, InternalError{}
	}

	result := make([]Result, 0, len(urls))
	for _, url := range urls {
		var res Result
		info, ok := l.cache.Load(url)
		if !ok {
			res = Result{Err: NotFoundError(nfError)}
			result = append(result, res)
			continue
		}

		e, ok := info.(entry)
		if !ok {
			res = Result{Err: InternalError{}}
			result = append(result, res)
			continue
		}

		if e.status == inProgress {
			res = Result{Err: NotReadyError(notReady)}
		} else {
			res = e.Result
		}
		result = append(result, res)
	}

	resp, err := json.Marshal(result)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func (l *loader) PushToQueue(urls []string) uint64 {
	rId := l.getNextId()
	l.requests.Store(rId, urls)

	go l.loadInfo(urls)

	return rId
}

func (l *loader) getNextId() uint64 {
	l.idMu.Lock()
	defer l.idMu.Unlock()
	if l.idCounter == requestsLimit {
		l.idCounter = 0
	}
	l.idCounter++

	return l.idCounter
}

func (l *loader) loadInfo(urls []string) {
	initVal := entry{status: inProgress}
	for _, url := range urls {
		e, loaded := l.cache.LoadOrStore(url, initVal)
		if !loaded {
			go l.load(url)
			continue
		}
		if v, ok := e.(entry); !ok || v.Err != nil {
			l.cache.Store(url, initVal)
			go l.load(url)
		}
	}
}

func (l *loader) load(url string) {
	resp, err := http.Get(url)
	if err != nil {
		l.saveError(url, InternalError{})
		return
	}
	defer resp.Body.Close()

	switch resp.StatusCode {
	case http.StatusOK:
	case http.StatusNotFound:
		l.saveError(url, NotFoundError(nfError))
		return
	default:
		l.saveError(url, BadResponseCode{})
		return
	}

	result, err := l.parseResponse(resp.Body, url)
	if err != nil {
		l.saveError(url, InternalError{})
		return
	}

	l.saveResult(url, result)
}

func (l *loader) saveError(url string, err error) {
	l.cache.Store(url, entry{status: finished, Result: Result{Err: err}})
}

func (l *loader) saveResult(url string, result Result) {
	l.cache.Store(url, entry{status: finished, Result: result})
}

func (l *loader) parseResponse(resp io.Reader, url string) (result Result, err error) {
	doc, err := goquery.NewDocumentFromReader(resp)
	if err != nil {
		return
	}

	result.Url = url
	result.Meta.InStock = doc.Find("#availability span").HasClass("a-color-success")
	result.Meta.Title = strings.TrimSpace(doc.Find("#productTitle").Text())

	l.parsePrice(&result, doc)
	if result.Err != nil {
		return
	}

	l.parseImage(&result, doc)
	if result.Err != nil {
		return
	}

	return
}

func (l *loader) parsePrice(result *Result, doc *goquery.Document) {
	var price string
	for i := 0; i < len(priceSelectors) && price == ""; i++ {
		price = doc.Find(priceSelectors[i]).Text()
	}

	if price == "" {
		result.Err = ParseError{"Can't find price"}
		return
	}
	result.Meta.Price = price
}

func (l *loader) parseImage(result *Result, doc *goquery.Document) {
	var attrValues []string
	doc.Find("#imageBlock img").Each(func(i int, selection *goquery.Selection) {
		val, ok := selection.Attr("data-a-dynamic-image")
		if !ok {
			return
		}
		attrValues = append(attrValues, val)
	})

	var (
		img    string
		images = map[string][]int{}
	)

	err := json.Unmarshal([]byte(attrValues[0]), &images)
	if err != nil || len(images) == 0 {
		result.Err = ParseError{"Can't find image url"}
		return
	}

	var maxV int
	for k, v := range images {
		if v[0] > maxV {
			img = k
			maxV = v[0]
		}
	}

	result.Meta.Image = img
}
